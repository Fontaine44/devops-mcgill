FROM rust:1.75-alpine3.19 AS build
WORKDIR /usr/src/app
COPY . .
RUN cargo install --path .
EXPOSE 9999
CMD ["cargo", "run"]

FROM alpine:3.19.1 AS final
#...
